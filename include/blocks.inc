<?php
/**
 * @file
 * QMO Bugzilla Dashboard - Blocks
 * @author Paul Craciunoiu
 */

function qmo_bugzilla_block_add() {
  $block_count = variable_get('qmo_bugzilla_block_count', 0);
  variable_set('qmo_bugzilla_block_count', $block_count+1);
  $reply = array(
                 array('block-added', 'vars' => array($block_count+1)) );
  set_messages($reply);
  $goto = get_first_nonempty($_SERVER['HTTP_REFERER'], 'qmo_bugzilla');
  drupal_goto($goto);
}
function qmo_bugzilla_block_remove() {
  $block_count = variable_get('qmo_bugzilla_block_count', 0);
  if ($block_count) {
    variable_set('qmo_bugzilla_block_count', $block_count-1);
    $title = variable_get('qmo_bugzilla_block_name_' . $block_count, 'Bugzilla Chart ' . $block_count);
    $reply = array(
               array('block-removed', 'vars' => array($title)) );
    variable_del('qmo_bugzilla_block_name_' . $block_count);
    variable_del('qmo_bugzilla_block_height_' . $block_count);
    variable_del('qmo_bugzilla_block_chart_id_'. $block_count);

  }
  else {
    $reply = array(
               array('block-remove-fail'));
  }
  set_messages($reply);
  $goto = get_first_nonempty($_SERVER['HTTP_REFERER'], 'qmo_bugzilla');
  drupal_goto($goto);
}

/** Similar to qmo_bugzilla_chart_manage_list()
 * @see qmo_bugzilla_chart_manage_list()
 */
function qmo_bugzilla_chart_block_list() {
  $sharing_condition = "";
  //admin? if not, check sharing.
  if (!user_access(QMO_BUGZILLA_ADMINISTER)) {
    global $user;
    $sharing_condition = " WHERE ";
    $valid_roles = user_roles(FALSE, QMO_BUGZILLA_ACCESS);
    foreach($user->roles as $rid => $role) {
      if (in_array($role, $valid_roles)) {
        $sharing_condition .= " (sharing LIKE '%[" . $rid . ":1]%') OR";
      }
    }
    $sharing_condition .= ' ( user_id = ' . $user->uid . ' )';
  }
  $query = "SELECT id, title, user_id FROM {" . QMO_BUGZILLA_TABLE_CHART . "} " . $sharing_condition . " ORDER BY added DESC";
  $result = db_query($query);
  if (!$result) {
    $reply[] = array('fail-charts');
    set_messages($reply);
    return FALSE;
  }
  else {
    $charts = array();
    while ($row = db_fetch_array($result)) {
      $charts[$row['id']] = $row['title'] . t(' (by !username)', array('!username' => user_load($row['user_id'])->name) );
    }
  }
  return $charts;
}
/**
 * Implementation of hook_block().
 */
function qmo_bugzilla_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;
  
  switch ($op) {
    case 'list':
      for ($i = 1; $i <= variable_get('qmo_bugzilla_block_count', 0); $i++) {
        $blocks[$i]['info'] = variable_get('qmo_bugzilla_block_name_' . $i, 'Bugzilla Chart '. $i) .' (Bugzilla Chart)';
      }
      return $blocks;
    break;

    case 'configure':
      if (!user_access(QMO_BUGZILLA_ACCESS)) {
        drupal_access_denied();
        return;
      }
      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Block description'),
                      '#default_value' => variable_get('qmo_bugzilla_block_name_'. $delta, 'Bugzilla Chart '. $delta),
        '#maxlength' => 64,
        '#description' => t('A brief description of your block. Used on the <a href="@overview">block overview page</a>.', array('@overview' => url('admin/build/block'))),
        '#required' => TRUE,
        '#weight' => -19,
      );
      $form['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#description' => t('The height of the chart in the block.'),
        '#default_value' => variable_get('qmo_bugzilla_block_height_' . $delta, variable_get('qmo_bugzilla_block_height_default', CHART_BLOCK_HEIGHT_DEFAULT)),
      );
      $form['chart'] = array(
        '#type' => 'select',
        '#title' => t('Chart'),
        '#description' => t('The chart to include in this block.'),
        '#default_value' => variable_get('qmo_bugzilla_block_chart_id_' . $delta, 0),
        '#options' => qmo_bugzilla_chart_block_list(),
      );
      return $form;
    break;

    case 'save':
      //print_r($edit);
      variable_set('qmo_bugzilla_block_name_'. $delta, $edit['name']);
      variable_set('qmo_bugzilla_block_height_'. $delta, $edit['height']);
      variable_set('qmo_bugzilla_block_chart_id_'. $delta, $edit['chart']);
    break;

    case 'view':
      $id = variable_get('qmo_bugzilla_block_chart_id_' . $delta, NULL);
      if (!is_null($id) && !qmo_bugzilla_chart_exists($id, FALSE)) {
        $block['content'] = FALSE;
        $block['subject'] = 'CHART NOT FOUND';
      }
      else {
        $chart = get_chart_info($id);
        $subject = $chart['title'];
        //timeplot init:
        if ($chart['type'] == CHART_TYPE_TIMEPLOT) {
          $timeplot = variable_get('qmo_bugzilla_timeplot_object', NULL);
          if (is_null($timeplot)) {
            $timeplot = new QMOTimeplot();
          }
        }
        else {
          $timeplot = NULL;
        }
        $chart['height'] = variable_get('qmo_bugzilla_block_height_' . $delta, variable_get('qmo_bugzilla_block_height_default', CHART_BLOCK_HEIGHT_DEFAULT));
        $chart_display = _build_chart($chart, $timeplot, 'block');
        if (!is_null($timeplot)) {
          variable_set('qmo_bugzilla_timeplot_object', $timeplot);
        }
        $block['content'] = $chart_display;
        $block['subject'] = '<span class="qmo-bugzilla-block-title">'. check_plain($subject) .'</span>';
      }
      return $block;
    break;
  }
}


/**
 * Generate HTML for the qmo_bugzilla block
 * @param op the operation from the URL
 * @param delta offset
 * @returns block HTML
 */
/*
function qmo_bugzilla_block($op='list', $delta=0) {
  // listing of blocks, such as on the admin/block page
  if ($op == "list") {
    $block[0]["info"] = t("QMO Bugzilla");
    return $block;
  }
  else if ($op == 'view') {
  // our block content
    // content variable that will be returned for display
    $block_content = '';

    // Get today's date
    $today = getdate();

    // calculate midnight one week ago
    $start_time = mktime(0, 0, 0, $today['mon'],
      ($today['mday'] - 7), $today['year']);

    // we want items that occur only on the day in question, so
    //calculate 1 day
    $end_time = $start_time + 86400;
    // 60 * 60 * 24 = 86400 seconds in a day
    $limitnum = variable_get("qmo_bugzilla_maxdisp", 3);

    $query = "SELECT nid, title, created FROM " .
             "{node} WHERE created >= %d " .
             "AND created <= %d";
    $result = db_query_range($query, $start_time, $end_time, 0, $limitnum);

    while ($links = db_fetch_object($result)) {
      $block_content .= l($links->title, 'node/' . $links->nid) . '<br />';
    }
    // check to see if there was any content before setting up the block
    if ($block_content == '') {
      // no content from a week ago, return nothing.
      $block['subject'] = 'QMO Bugzilla';
      $block['content'] = 'Nothing found';  
      return $block;
    }
    // add a more link to our page that displays all the links
      $block_content .=
        "<div class=\"more-link\">" .
        l(
          t("more"),
          "qmo_bugzilla",
          array("attributes" => array(
            "title" => t("More... "))
          )
        ) . "</div>";

    // set up the block
    $block['subject'] = 'QMO Bugzilla';
    $block['content'] = $block_content;
    return $block;
  }
}
*/