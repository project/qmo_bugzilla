<?php
/**
 * @file
 * QMO Bugzilla Dashboard - Access functions.
 * @author Paul Craciunoiu
 */

/**
 * Checks view permissions for a given chart
 * @param chart array, requires:
 *        -- id
 *        -- user_id
 * @param user (optional) drupal user object
 */
function qmo_bugzilla_access_chart_view($chart, $user = NULL) {
  $access = FALSE;
  if (is_null($user)) {
    global $user;
  }
  //top -> down
  if (user_access(QMO_BUGZILLA_ADMINISTER)) {
    $access = TRUE;
  }
  elseif (qmo_check_sharing($chart['id'])) {
    $access = TRUE;
  }
  elseif (($chart['user_id'] == $user->uid) && (user_access(QMO_BUGZILLA_ACCESS))) {
    $access = TRUE;
  }
  if(!$access) {
    drupal_access_denied();
    return FALSE;
  }
}

function qmo_bugzilla_access_chart_empty($chart, $user = NULL) {
  $access = FALSE;
  if (is_null($user)) {
    global $user;
  }
  //top -> down
  if (user_access(QMO_BUGZILLA_ADMINISTER)) {
    $access = TRUE;
  }
  elseif (qmo_check_sharing($chart['id']) && QMO_BUGZILLA_REMOVE_ANY) {
    $access = TRUE;
  }
  elseif (($chart['user_id'] == $user->uid) && (user_access(QMO_BUGZILLA_REMOVE_OWN))) {
    $access = TRUE;
  }
  if(!$access) {
    drupal_access_denied();
    return FALSE;
  }
}

function qmo_bugzilla_access_chart_edit($chart, $user = NULL) {
  $access = FALSE;
  if (is_null($user)) {
    global $user;
  }
  //top -> down
  if (user_access(QMO_BUGZILLA_ADMINISTER)) {
    $access = TRUE;
  }
  elseif (user_access(QMO_BUGZILLA_EDIT_ANY)) {
    $access = TRUE;
  }
  elseif (qmo_check_sharing($chart['id']) && QMO_BUGZILLA_EDIT_SHARED) {
    $access = TRUE;
  }
  elseif (($chart['user_id'] == $user->uid) && (user_access(QMO_BUGZILLA_EDIT_OWN))) {
    $access = TRUE;
  }
  if(!$access) {
    drupal_access_denied();
    return FALSE;
  }
}