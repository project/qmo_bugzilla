<?php
/**
 * @file
 * QMO Bugzilla Dashboard - Constants
 * @author Paul Craciunoiu
 */

/* -----------------------------------------------------------------

  Constants

------------------------------------------------------------------ */
define('XML_TAG_COUNT_OTHER', 'other');
define('LABEL_TOTAL', 'Total Count');
define('QMO_BUGZILLA_TIMEZONE', 'GMT-0700');//Nov 22 2004 14:24:10 GMT-0400
define('DATE_TIMEPLOT_FORMAT', 'M d Y H:i:s');

// permissions names
define('QMO_BUGZILLA_ACCESS', 'access qmo_bugzilla charts');
define('QMO_BUGZILLA_ACCESS_DASH', 'access qmo_bugzilla dashboard');
define('QMO_BUGZILLA_CREATE', 'create & manage own qmo_bugzilla charts');
define('QMO_BUGZILLA_EDIT_OWN', 'edit own qmo_bugzilla charts');
define('QMO_BUGZILLA_EDIT_SHARED', 'edit shared qmo_bugzilla charts');
define('QMO_BUGZILLA_EDIT_ANY', 'edit any qmo_bugzilla charts');
define('QMO_BUGZILLA_REMOVE_OWN', 'remove own qmo_bugzilla charts');
define('QMO_BUGZILLA_REMOVE_ANY', 'remove any qmo_bugzilla charts');
define('QMO_BUGZILLA_RUN_OWN', 'run own qmo_bugzilla charts');
define('QMO_BUGZILLA_RUN_ANY', 'run any qmo_bugzilla charts');
define('QMO_BUGZILLA_ADMINISTER', 'administer qmo_bugzilla');
define('QMO_BUGZILLA_MANAGE_ANY', 'manage any qmo_bugzilla charts');
define('QMO_BUGZILLA_ADD_REM_BLOCK', 'add & remove qmo_bugzilla blocks');
// table names
define('QMO_BUGZILLA_TABLE_CHART',             'qmo_bugzilla_chart');
define('QMO_BUGZILLA_TABLE_MY_CHART',             'qmo_bugzilla_my_chart');
define('QMO_BUGZILLA_TABLE_QUERY',             'qmo_bugzilla_query');
define('QMO_BUGZILLA_TABLE_RESULT',           'qmo_bugzilla_result');
define('QMO_BUGZILLA_TABLE_TIMEPLOT',       'qmo_bugzilla_timeplot');
define('QMO_BUGZILLA_TABLE_WEIGHT',       'qmo_bugzilla_weight');
// xml constants
define('XML_TAGS',
"BZ:ID
BZ:ALIAS
BZ:OPENDATE
BZ:CHANGEDDATE
BZ:BUG_SEVERITY
BZ:PRIORITY
BZ:REP_PLATFORM
BZ:ASSIGNED_TO
BZ:ASSIGNED_TO_REALNAME
BZ:REPORTER
BZ:REPORTER_REALNAME
BZ:BUG_STATUS
BZ:RESOLUTION
BZ:CLASSIFICATION
BZ:PRODUCT
BZ:COMPONENT
BZ:VERSION
BZ:OP_SYS
BZ:VOTES
BZ:TARGET_MILESTONE
BZ:QA_CONTACT
BZ:STATUS_WHITEBOARD
BZ:KEYWORDS
BZ:SHORT_DESC");
define('MAINTAG', "BZ:BUG");
define('XML_TAG_ID', "BZ:ID");
define('DATA_TABLE', 'bugs');
define('SEPARATOR', ', ');
define('LIST_BUG_URL', 'https://bugzilla.mozilla.org/buglist.cgi?bug_id=');
define('BUG_ID_URL', 'https://bugzilla.mozilla.org/show_bug.cgi?id=');
define('ID_TAG', 'BZ:ID');

// chart defaults
define('SIZE_ROWS', 3);
define('SIZE_COLS', 3);
define('DIV', 'data-');
define('DEFAULT_FREQUENCY', '0000-00-01 00:00:00');
define('DEFAULT_QUERY_URL', 'http://');
define('DATE_SQL_FORMAT', 'Y-m-d H:i:s');
define('CHART_TYPE_TIMEPLOT', -1);
define('CHART_TYPE_BAR', 0);
define('CHART_TYPE_PIE', 1);
define('CHART_HEIGHT_DASH_DEFAULT', '150px');
define('CHART_HEIGHT_DEFAULT', '150px');
define('CHART_BLOCK_HEIGHT_DEFAULT', '150px');
define('CHART_WIDTH_DEFAULT', '100%');
define('DEFAULT_REDIRECT', 'qmo_bugzilla');
define('OPENFLASHMAP_BAR', 'vbar2D');
define('OPENFLASHMAP_PIE', 'pie2D');
define('MAPPING_SEPARATOR', '|');
// extensions defaults
define('EXT_COUNTS', '.txt');
define('EXT_EVENTS', '.xml');

/**
 * Returns first non-NULL value
 */
function get_first_nonempty() {
  $args = func_get_args();
  if (!is_array($args)) {
    return $args;
  }
  foreach ($args as $value) {
    if (isset($value)) return $value;
  }
}

function get_value($name) {
  switch($name) {
    case "UNMATCHED":
      return array('Group all as "other"',
                   'Show each as distinct',
                   "Don't show",
                  );
    case "start_date":
    case "cron_start_date":
      return date("Y-m-d");
    case "end_date":
    case "cron_end_date":
      return get_tomorrow("Y-m-d");
    case "chart_manage_options":
      return array(
                   -1 => t('Change type to timeplot'),
                           0 => t('Change type to bar'),
                                  1 => t('Change type to pie'),
                                         5 => t('Empty'),
                                             6 => t('Delete'),
              //7 => t('Make private'),
              8 => t('Share/Make private'),
                     9 => t('Remove alias'),
                            10 => t('Add to "My charts"'),
                                    11 => t('Remove from "My charts"'),
                  );
    case "type:-1":
      return t('Timeplot');
    case "type:0":
      return t('Bar');
    case "type:1":
      return t('Pie');
  }
}

function generate_timeplot_prefix($id) {
  return 
      '#
# Automatically generated data file for SIMILE Timeplot
# Source: Bugzilla Dashboard module
# Chart id: ' . $id . '
#
';
}
function generate_timeplot_event_prefix($id) {
  return 
      '<!--
Automatically generated data file for SIMILE Timeplot
Source: Bugzilla Dashboard module
Chart id: ' . $id . '
-->
';
}

/**
 * Holds all drupal_set_message calls except for the one in hook_install.
 * @param $codenames array of message codes (string format) that will set a message, case by case
 * --------- each $code (elem of array) is an array with
 * ------------ $code[0] as the message identifier
 * ------------ $code['vars'] as additional variables to use in the message.
 */
function set_messages($codenames = array()) {
  if (!$codenames) {
    $codenames = array();
  }
  foreach ($codenames as $code) {
    switch($code[0]) {
      case 'chart-edit':
        drupal_set_message( t("Chart %title updated.", array('%title' => $code['vars'][0])) );
        break;
      case 'chart-add':
        drupal_set_message( t("Added chart %title.", array('%title' => $code['vars'][0])));
        break;
      case 'fail-query-delete':
        drupal_set_message( t('Database error, failed to delete old queries.'), 'error');
        break;
      case 'fail-query-update':
        drupal_set_message( t('Database error, failed to update queries.'), 'error');
        break;
      case 'fail-chart-update':
        drupal_set_message( t('Database error, failed to update chart'), 'error');
        break;
      case 'fail-chart-add':
        drupal_set_message( t('Database error, failed to add chart'), 'error');
        break;
      case 'fail-chart-id':
        drupal_set_message( t('Error: failed to find the chart id.'));
        break;
      case 'empty-query':
        drupal_set_message( t("At least one query has no results."), 'warning');
        break;
      case 'no-query':
        drupal_set_message( t('No queries found. '), 'warning');
        break;
      case 'no-query-enabled':
        drupal_set_message( t("No enabled queries found."), 'warning');
        break;
      case 'empty-result':
        drupal_set_message( t('Empty results.', 'warning'));
        break;
      case 'no-data':
        drupal_set_message( t('Error: Data not found. '), 'error');
        break;
      case 'no-datafile':
        drupal_set_message( t('Error: Data file not found. '), 'error');
        break;
      case 'no-chart':
        drupal_set_message( t("Error: chart not found. "), 'error');
        break;
      case 'fail-chart-empty':
        drupal_set_message( t("Warning: Failed to empty chart."), 'warning');
        break;
      case 'fail-result-insert':
        drupal_set_message( t("Error inserting results."), 'error');
        break;
      case 'chart-emptied':
        drupal_set_message( t("Data removed, chart %title emptied.", array('%title' => $code['vars'][0])) );
        break;
      case 'remove-chart':
        drupal_set_message( t("Chart %title successfully removed.", array('%title' => $code['vars'][0])) );
        break;
      case 'curl-404':
        drupal_set_message( t("Error: 404 status received on fetching URL page content at @url.", array('@url' => $code['vars'][0])) );
        break;
      case 'xml-error':
        drupal_set_message( t("XML error: !s at line !d. <br />
            The URL was: !url", array(
                                      '!s' => $code['vars'][0],
                                      '!d' => $code['vars'][1],
                                      '!url' => check_url($code['vars'][2]),
                                     )), 'error' );
        break;
      case 'fail-charts':
        drupal_set_message( t('Error: failed to get charts.'), 'error');
        break;
      case 'fail-weights-update':
        drupal_set_message( t('Error: Weights update failed.'), 'error');
        break;
      case 'query-add':
        drupal_set_message( t('Added another query.') );
        break;
      case 'query-remove':
        drupal_set_message( t('Removed last query.') );
        break;
      case 'fail-query-remove':
        drupal_set_message( t('Chart must have at least one query.'), 'warning');
        break;
      case 'fail-query-run':
        drupal_set_message( t('Failed to get queries.'), 'error');
        break;
      case 'no-tags':
        drupal_set_message(t('Error, no tags found!'), 'error');
        break;
      case 'file-read-error':
        drupal_set_message( t("Error reading file data. Path: !path", array('!path' => $code['vars'][0])), 'error' );
        break;
      case 'file-write-error':
        drupal_set_message( t("Can't open file: !file", array('!file' => $code['vars'][0])), 'error');
        break;
      case 'empty-xml-tag':
        drupal_set_message( t("An empty XML tag found on line !line in the settings", array('!line' => $code['vars'][0])), 'error');
        break;
      case 'remove-chart-deny':
        drupal_set_message( t("You do not have the permissions required to remove this chart.") );
        break;
      case 'block-added':
        drupal_set_message( t('Block %title was added. Configure it !here.', array('%title' => 'Bugzilla Chart ' . $code['vars'][0], '!here' => l(t('here'), 'admin/build/block/configure/qmo_bugzilla/' . $code['vars'][0]))) );
        break;
      case 'block-removed':
        drupal_set_message( t('Block %title was removed.', array('%title' => $code['vars'][0])) );
        break;
      case 'block-remove-fail':
        drupal_set_message( t('No Bugzilla Chart blocks to remove!' ), 'error' );
        break;
      case 'manage-nothing':
        drupal_set_message( t('No chart selected'), 'error' );
        break;
      case 'missing-drupalpath':
        drupal_set_message( t('Drupal path is not set, please set it !here.', array('!here' => l(t('here'), 'admin/settings/qmo_bugzilla'))), 'error' );
        break;
      case 'missing-fullpath':
        drupal_set_message( t('Full path is not set, please set it !here.', array('!here' => l(t('here'), 'admin/settings/qmo_bugzilla'))), 'error' );
        break;
      case 'missing-relpath':
        drupal_set_message( t('Relative path is not set, please set it !here.', array('!here' => l(t('here'), 'admin/settings/qmo_bugzilla'))), 'error' );
        break;
      default:
        print_r($code);
        drupal_set_message( t('Unknown message'), 'error');
    }
  }
}

function qmo_bugzilla_chart_display_empty($height = '') {
  if ($height) {
    $height = 'style="height:' . $height . '"';
  }
  return '<div class="empty-chart" ' . $height . '>' . t('Empty.') . '</div>';
}
