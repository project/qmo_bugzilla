<?php
/**
 * @file
 * QMOTimeplot object designed for QMO, http://quality.mozilla.org
 * @author Paul Craciunoiu
 * @contact gewissen[at]gmail[dot]com
 * @site http://quality.mozilla.org
 *
 * Summary:
 * Administer a timeplot script.
 */
class QMOTimeplot {
// ------------------------------ CUSTOM COLORS --------------------------
  var $LINECOLORS = array(
    "#ff0000",
    "#aa0000",
    "#880000",
    "#440000",
    "#000000",
    "#00ff00",
    "#0f0f00",
    "#0ff000",
    "#000ff0",
    "#00000f",
    "#880000",
    "#aa0000",
    "#ff0000",
    "#aa0000",
    "#880000",
    "#440000",
    "#000000",
    "#00ff00",
    "#0f0f00",
    "#0ff000",
    "#000ff0",
    "#00000f",
    "#880000",
    "#aa0000",
  );
  var $FILLCOLORS = array(
    "#cc8080",
    "#80cc80",
    "#8080cc",
  );
  var $GRIDCOLOR = '#000000';
  var $EVENT_LINECOLOR = '#0000ff';

// ------------------------------ CONSTANTS ------------------------------
  var $START = '
  <script type="text/javascript">';
  var $END = '
  </script>';
  var $ONLOAD_START = '
    function onLoad() {';
  var $ONLOAD_END = '
    }';
  var $ONRESIZE_START = '
    var resizeTimerID = NULL;
    function onResize() {
        if (resizeTimerID == NULL) {
            resizeTimerID = window.setTimeout(function() {
                resizeTimerID = NULL;';
  var $ONRESIZE_END = '
            }, 100);
        }
    }';

// ------------------------------ VARIABLES ------------------------------
  var $vars = "";
  var $eventsource = "";
  var $timegeometry = "";
  var $valuegeometry = "";
  var $plotinfo = "";
  var $vars_load = "";
  var $onresize = "";

/**
 * Returns the Javascript to be included on the page.
 */
function get_script() {
  return $this->START .
          $this->vars .
          $this->ONLOAD_START .
          $this->eventsource .
          $this->timegeometry .
          $this->valuegeometry .
          $this->plotinfo .
          $this->vars_load .
          $this->ONLOAD_END .
          $this->ONRESIZE_START .
          $this->onresize .
          $this->ONRESIZE_END .
          $this->END;
}

/**
  * Generates script for displaying charts on the page.
  * @param id the chart id
  * @param div the div element to be populated with the chart
  * @param data_file the fullpath to the data file, WITHOUT extension (automatically set as EXT_COUNTS & EXT_EVENTS)
  * @param xml TRUE if adding events, FALSE otherwise, defaults to TRUE
  * @param columns number of columns the data file will have, equal to how many queries the chart has
  */
function add_chart($id, $div, $data_file, $xml = TRUE, $columns = 1 ) {
  $unique_time = mktime() . rand();
  $this->vars .= "
  var timeplot_". $id . $unique_time . ";";

  $this->eventsource .= "
  var eventSource_". $id . $unique_time . " = new Timeplot.DefaultEventSource();";

  $this->timegeometry .= '
  var timeGeometry_'. $id . $unique_time . ' =  new Timeplot.DefaultTimeGeometry({
      gridColor: new Timeplot.Color("' . $this->GRIDCOLOR . '"),
      axisLabelsPlacement: "top"
    });';

  $this->valuegeometry .= '
  var valueGeometry_'. $id . $unique_time . ' =  new Timeplot.DefaultValueGeometry({
    gridColor: "' . $this->GRIDCOLOR . '",
    min: 0
  });';
  
  $this->plotinfo .= '
  var plotInfo_'. $id . $unique_time . ' =  [';
  $fillcolor = 'fillColor: "' . $this->FILLCOLORS[$count % 3] . '",';
  for ($count=1; $count<=$columns; $count++) {
    $this->plotinfo .= '
    Timeplot.createPlotInfo({
      id: "plot' . $count . '",
      dataSource: new Timeplot.ColumnSource( eventSource_'. $id . $unique_time . ',' . $count . '),
    timeGeometry: timeGeometry_'. $id . $unique_time . ',
      valueGeometry: valueGeometry_'. $id . $unique_time . ',
      lineColor: "' . $this->LINECOLORS[$count % 24] . '",
      showValues: true
    }),';
    $fillcolor="";
  }
  if ($xml) {
    $this->plotinfo .= '
  Timeplot.createPlotInfo({
  id: "plotxml",
    timeGeometry: timeGeometry_'. $id . $unique_time . ',
    eventSource: eventSource_'. $id . $unique_time . '_xml,
    lineColor: "' . $this->EVENT_LINECOLOR . '"
  })';
    $this->eventsource .= "
  var eventSource_". $id . $unique_time . "_xml = new Timeplot.DefaultEventSource();";
  }
  else {
    //remove last comma
    $this->plotinfo = substr($this->plotinfo, 0, -1);
  }
  $this->plotinfo .= '
  ];';

  $this->vars_load .= '
  timeplot_'. $id . $unique_time . ' = Timeplot.create(document.getElementById("'. $div . $id . '"), plotInfo_'. $id . $unique_time . ');
  timeplot_'. $id . $unique_time . '.loadText("'. $data_file . EXT_COUNTS . '", ",", eventSource_'. $id . $unique_time . ');
  timeplot_'. $id . $unique_time . '.loadXML("'. $data_file . EXT_EVENTS . '", eventSource_'. $id . $unique_time . '_xml);';

  $this->onresize .= '
          if (timeplot_'. $id . $unique_time . ') timeplot_'. $id . $unique_time . '.repaint();';
}
}