<?php
/**
 * @file
 * QMO Bugzilla Dashboard - XML Parser object
 * XML Parser designed for QMO, http://quality.mozilla.org, inspired by sitepoint.com
 * @author Paul Craciunoiu
 * @contact gewissen[at]gmail[dot]com
 * @site http://quality.mozilla.org
 * @site http://www.sitepoint.com/article/php-xml-parsing-rss-1-0
 *
 * Summary:
 * Parse given data according to some options (mapping, counts, etc)
 */
class XMLParser {

/******************** INITIALIZABLE *********************************/
  //which tags to look at?
  var $tags = array();
  //what mapping to perform?
  var $map = array();
  //to what value do we map?
  var $map_values = array();
  //what are the main items?
  var $maintag;
  //how do we identify each main item?
  var $id_tag;
  //group all unmatched mappings option
  var $unmatched_option = 0;
  //do the mapping?
  var $do_map = FALSE;
/******************** INTERNAL *************************************/
  // TRUE if inside $tagname
  var $inside = FALSE;
  //current mainitem data
  var $row = array();

/******************** RETURN VALUES *********************************/
  // total tags parsed
  var $tags_parsed = 0;
  //this returns all the IDs parsed, comma separated
  var $ids = "";
  //these return IDs and counts for each mapping (empty array if no mapping)
  var $map_ids = array();
  var $mapping_counts = array();

/**
 * Callback on <tag>
 * @param parser xml parser
 * @param tagname <tag> (without the '<', '>'
 * @param attrs tag attributes (unused)
 */
function startElement($parser, $tagname, $attrs) {
  if ($this->inside) {
    $this->tag = $tagname;
  } elseif ($tagname == $this->maintag) {
    $this->inside = TRUE;
  }
}

/**
 * Callback for </tag>
 * @param parser xml parser
 * @param tagname <tag> (without the '<', '>'
 */
function endElement($parser, $tagname) {
  if ($tagname == $this->maintag) {
    if ($this->do_map) $this->map_row();
    foreach ($this->tags as $value) {
      if ($value == $this->id_tag) {
        $this->ids .= $this->row[$value].",";
      }
      $this->row[$value] = "";
    }
    $this->inside = FALSE;
    $this->tags_parsed++;
  }
}

/**
 * Callback for parsing data. Called once for each line.
 * @param parser xml parser
 * @param data data on the line
 */
function characterData($parser, $data) {
  if ($this->inside) {
    if (trim($data) != $this->check_empty($this->tag, $data) && in_array($this->tag, $this->tags)) {
      $this->row[$this->tag] .= htmlentities(trim($data), ENT_QUOTES);
    }
  }
}

/**
 * Counts the tag by its data value.
 */
function count_tag($key, $data_value) {
  if ($this->mapping_counts[$key][$data_value]) {
    $this->mapping_counts[$key][$data_value]++;
  }
  else {
    $this->mapping_counts[$key][$data_value] = 1;
  }
}

/**
 * Counts the row and remembers the id
 */
function count_row($map_value) {
  if ($this->mapping_counts[$map_value]) {
    $this->mapping_counts[$map_value]++;
    $this->map_ids[$map_value] .= $this->row[$this->id_tag].",";
  }
  else {
    $this->mapping_counts[$map_value] = 1;
    $this->map_ids[$map_value] = $this->row[$this->id_tag].",";
  }
}

/**
 * Checks if a tag is considered to be empty. Returns TRUE or FALSE.
 * @param tagname tag
 * @param data data inside of the tag
 */
function check_empty($tagname, $data) {
  switch ($tagname) {
    default:
      if ($data = '') {
        return TRUE;
      }
      return FALSE;
      break;
  }
}

/**
 * Assumes the following mapping format:
 * $tags = array(TAGNAME_1, TAGNAME_2 ... TAGNAME_N)
 * $map[i] = array(TAG_V(i)_1, TAG_V(i)_2 ... TAG_V(i)_N)
 * For the current row, it checks if ($row[tags[k]] == $map[i][k]) for all k (columns) in tags (and map[i]), and then for all i (rows in map)
 * Make sure to pop the last column (values to map to) from each row
 */
function map_row() {
  $num_rows = count($this->map);
  $match = TRUE;
  if ($num_rows) {
    for($i=0; $i<$num_rows; $i++) {
      //assume it's a match
      $j = 0;
      foreach($this->tags as $tagname) {
        if ($tagname != $this->id_tag) {
            $match = preg_match('/' . $this->map[$i][$j] . '/', $this->row[$tagname]);
            if (!$match) {
              break;
            }		
        }
        $j++;
      }
      if ($match) {
        $this->count_row($this->map_values[$i]);
        return;
      }
    }
    if(!$this->unmatched_option) {
        //count unmatched rows as "other"
        $this->count_row(XML_TAG_COUNT_OTHER);
    }
    elseif($this->unmatched_option == 1) {
        //count unmatched rows as distinct
        $new_row = $this->row;
        unset($new_row[$this->id_tag]);
        $map_string = implode('+', $new_row);
        $this->count_row($map_string);
        return; //we're done here
    }
    else {
        //unmatched rows will not be shown
        //do what?
    }
  }
  else {
	//show all distincts
	//array-to-string the values:
	$new_row = $this->row;
	unset($new_row[$this->id_tag]);
	$map_string = implode('+', $new_row);
	$this->count_row($map_string);
  }
}

}
