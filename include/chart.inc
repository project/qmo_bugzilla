<?php
/**
 * @file
 * QMO Bugzilla Dashboard - Charts functions.
 * @author Paul Craciunoiu
 * @contact gewissen[at]gmail[dot]com
 * @site http://quality.mozilla.org
 */
 
/************************************************* VIEW CHART *******************************************/
function qmo_bugzilla_chart_view($id) {
  if (!is_null($id) && !qmo_bugzilla_chart_exists($id, FALSE)) {
    return FALSE;
  }
  global $user;
  $chart = get_chart_info($id);
  //check access
  qmo_bugzilla_access_chart_view($chart);
  
  drupal_set_title($chart['title']);
  //timeplot init:
  if ($chart['type'] == CHART_TYPE_TIMEPLOT) {
    $timeplot = variable_get('qmo_bugzilla_timeplot_object', NULL);
    if (is_null($timeplot)) {
      $timeplot = new QMOTimeplot();
    }
  }
  else {
    $timeplot = NULL;
  }
  $chart_display = _build_chart($chart, $timeplot);
  if (!is_null($timeplot)) {
    variable_set('qmo_bugzilla_timeplot_object', $timeplot);
  }
  $output = '<span class="enabled-queries">' . t('Enabled queries shown below:') . '</span>';
  $header = array(
              array('data' => 'Query Title', 'field' => 'title'),
              array('data' => 'Query Description', 'field' => 'description' ),
              array('data' => 'Mapping', 'field' => 'mapping'),
              array('data' => 'Url'),
            );
  $result = db_query("SElECT * FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE chart_id = %d " . tablesort_sql($header), $id);
  $query_info = array();
  while ($row = db_fetch_array($result)) {
    $url = qmo_bugzilla_url_parameters(urldecode($row['url']));
    $url_output = "";
    foreach ($url as $param => $values) {
      $url_output .= '<span class="param-name">' . $param . '</span><span class="param-colon">: </span>';
      $url_output .= implode(', ', $values);
      $url_output .= '<br/>';
    }
    $query_info[] = array(
                      $row['title'],
                      $row['description'],
                      nl2br($row['mapping']),
                      $url_output,
                    );
  }
  //output the themed table
  $output .= theme_table($header, $query_info);
  return $chart_display . $output;
}

/** 
 * Returns max(count of results) over all of the chart's queries or FALSE if none.
 * - count, if there is at least one result for one query of the chart.
 * - FALSE, otherwise
 * - NULL, if chart doesn't exist
 * @param id chart id
 * @param enabled (optional) check only enabled queries,defaults to FALSE
 * @return (int) max(count of results)
 */
function qmo_bugzilla_chart_hasdata($id, $enabled = FALSE) {
  if ($enabled) {
    $enabled = ' and enabled = 1 ';
  }
  else {
    $enabled = '';
  }
  $query = "SELECT * FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE chart_id = %d " . $enabled;
  $result = db_query($query, $id);
  $chart_results = array();
  $count = 0;
  //get results
  if ($result) {
    while ($row = db_fetch_object($result)) {
      $query_r = "SELECT COUNT(DISTINCT added) AS count FROM {" . QMO_BUGZILLA_TABLE_RESULT . "} WHERE query_id = %d";
      $result_r = db_query($query_r, $row->id);
      if ( ($result_r) && ($row_r = db_fetch_object($result_r)) ) {
        $count = ($count > $row_r->count) ? $count : $row_r->count ;
      }
    }
  }
  return $count;
}

/**
 * Checks if chart with given id exists, and if not, sets an error message and returns FALSE.
 * @param id chart id
 * @param silent don't set message, just return TRUE or FALSE
 */
function qmo_bugzilla_chart_exists($id = NULL, $silent = TRUE) {
  //check if chart exists
  if ($id == NULL) {
    return FALSE;
  }
  $query = "SELECT id FROM {" . QMO_BUGZILLA_TABLE_CHART . "} WHERE id = %d ";
  $result = db_query($query, $id);
  if (!($row = db_fetch_object($result))) {
    if (!$silent) {
      $reply[] = array('no-chart');
      set_messages($reply);
    }
    return FALSE;
  }
  return TRUE;  
}

/**
 * Gets and outputs the chart data.
 * @path qmo_bugzilla/chart/$id/data
 */
function qmo_bugzilla_chart_data($id) {
  qmo_bugzilla_access_chart_view($chart);
  $reply = array();
  $output = "";
  
  //get some chart info: title and type
  $query = "SELECT title, type FROM {" . QMO_BUGZILLA_TABLE_CHART . "} WHERE id = %d ";
  $result = db_query($query, $id);
  if ($row = db_fetch_object($result)) {
    $chart['title'] = $row->title;
    $chart['type'] = $row->type;
    drupal_set_title($chart['title']);
  }
  else {
    //chart not found
    $reply[] = array('no-chart');
    set_messages($reply);
    return FALSE;
  }
  $hasdata = qmo_bugzilla_chart_hasdata($id);
  if (!$hasdata) {
    $reply[] = array('no-data');
    set_messages($reply);
    return FALSE;
  }
  
  $output = "";
  
  //for timeplots, show timestamped data file
  $drupalpath = variable_get('qmo_bugzilla_chart_drupalpath', '');
  if (empty($drupalpath)) {
    $reply[] = array('missing-drupalpath');
    set_messages($reply);
    return FALSE;
  }
  $fullpath = variable_get('qmo_bugzilla_chart_fullpath', '');
  if (empty($fullpath)) {
    $reply[] = array('missing-fullpath');
    set_messages($reply);
    return FALSE;
  }
  if ($chart['type'] == CHART_TYPE_TIMEPLOT) {
    $timestamp = variable_get('qmo_bugzilla_timestamp_' . $id, '');
    $data_file = DIV . $id . '-' . $timestamp . EXT_COUNTS;
    if (file_exists($fullpath . $data_file)) {
      $output .= '<div class="fltrt">' . l(t('Download data'), $drupalpath . $data_file) . ' | ' .
          l(t('Delete all data'), 'qmo_bugzilla/chart/' . $id . '/empty', array( 'attributes' => array('title' => t('Keeps the chart but removes all query results')) )) . '</div>';
    }
    else {
      $reply[] = array('no-datafile');
    }
      }
  //for others, show a different data file - TODO
  else {
    $output .= '<div class="fltrt">'
        //. l(t('Download data'), $drupalpath . $data_file) . ' | '
        . l(t('Delete all data'), 'qmo_bugzilla/chart/' . $id . '/empty', array( 'attributes' => array('title' => t('Keeps the chart but removes all query results')) )) . '</div>';
      }
  
  //set up drupal sortable table - header
  $header = array(
              array('data' => 'Label', 'field' => 'label'),
              array('data' => 'Date', 'sort' => 'desc', 'field' => 'added', 'width' => "100px" ),
              array('data' => 'Count', 'field' => 'count'),
              array('data' => 'Bugs', 'field' => 'bugs')
            );
  //get queries and results for each query
  $query = "SELECT id, title, enabled FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE chart_id = %d ";
  $result = db_query($query, $id);
  $chart_results = array();
  $count = 0;
  //get results
  if ($result) {
    while ($row = db_fetch_object($result)) {
      $query_r = "SELECT * FROM {" . QMO_BUGZILLA_TABLE_RESULT . "} WHERE query_id = %d ";
      $result_r = db_query($query_r . tablesort_sql($header), $row->id);
      $count++;
      $chart_results[$row->id] = array();
      if ($result_r) {
        while ($row_r = db_fetch_object($result_r)) {
          $bugs_array = explode(',', $row_r->bugs);
          $buglist = "";
          foreach ($bugs_array as $bug_id) {
            $buglist .= l($bug_id, variable_get('qmo_bugzilla_bug_id_url', BUG_ID_URL) . $bug_id ) . ', ';
          }
          $buglist = substr($buglist, 0, -2);
          $chart_results[$row->id][] = array(
                                         $row_r->label,
                                         t( '!time ago', array('!time' => format_interval(time() - strtotime($row_r->added))) ),
                                         $row_r->count,
                                         $buglist
                                       );
        }
      }
      else {
        //no results found for this query
        $reply[] = array('empty-query');
      }
      if (!$row->enabled) {
        $disabled = t('(disabled)');
      }
      else {
        $disabled = '';
      }
      //output the themed table
      $output .= '<div>' . t('Query !count: "%title" !disabled', array('!count' =>$count, '%title' => $row->title, '!disabled' => $disabled) ) . '</div>' . theme_table($header, $chart_results[$row->id]);
    }
  }
  else {
    //no queries found
    $reply[] = array('no-query');
  }

  set_messages($reply);
  return $output;
}

/**
 * Gets the chart as an array.
 * @param id chart id
 * @return chart = array(...)
 */
function get_chart_info($id) {
  $query = "SELECT * FROM {" . QMO_BUGZILLA_TABLE_CHART . "} WHERE id = %d ";
  $result = db_query($query, $id);
  if ($result) {
    $chart = db_fetch_array($result);
    return $chart;
  }
  else {
    $reply = array( array('no-chart') );
    set_messages($reply);
    return array();
  } 
}
/**
 * Builds a chart based on the given parameters
 * @param chart the chart, containing:
 * -------- chart['id']
 * -------- chart['type']
 * -------- chart['title']
 * -------- chart['height']
 * -------- chart['width']
 * -------- chart['weight'] (optional)
 * -------- chart['added'] (optional)
 * -------- chart['updated'] (optional)
 * @param timeplot the timeplot object to add timeplot charts to. Pass by reference. (optional if no timeplot chart is built)
 * @param display_type last is true if the chart will be displayed on the dashboard or on its own page (may add 'block' later)
 */
function _build_chart($chart, &$timeplot = NULL, $display_type = NULL) {
  $relpath = variable_get('qmo_bugzilla_chart_relpath', '');
  if (empty($relpath)) {
    $reply[] = array('missing-relpath');
    set_messages($reply);
    return FALSE;
  }
  $fullpath = variable_get('qmo_bugzilla_chart_fullpath', '');
  if (empty($fullpath)) {
    $reply[] = array('missing-fullpath');
    set_messages($reply);
    return FALSE;
  }
  $reply = array();
  if ($display_type == 'dash') {
    $chart['height'] = variable_get('qmo_bugzilla_dash_chart_height', CHART_HEIGHT_DASH_DEFAULT);
    $chart['width'] = NULL;
  }
  elseif ($display_type == 'block') {
    $chart['width'] = NULL;
  }
  $hasdata = qmo_bugzilla_chart_hasdata($chart['id']);
  if ($hasdata) {
    //timeplot chart
    if ($chart['type'] == CHART_TYPE_TIMEPLOT) {
      if ($hasdata > 1) {
        $timestamp = variable_get('qmo_bugzilla_timestamp_' . $chart['id'], '');
        $data_file = DIV . $chart['id'] . '-' . $timestamp;
        $div = DIV . mktime() . rand() . '-';
        $column_count = get_timeplot_column_count($chart);
        if (file_exists($fullpath . $data_file . EXT_COUNTS) && file_exists($fullpath . $data_file . EXT_EVENTS)) {
            $timeplot->add_chart($chart['id'], $div, $relpath . $data_file, TRUE,  $column_count );
            $chart_display = get_timeplot_div($div . $chart['id'], $chart['height'], $chart['width']);
        }
        else {
            $reply[] = array('no-datafile');
            set_messages($reply);
            return FALSE;
        }
      }
      else {
        $chart_display = t('No chart available yet. Timeplot needs at least 2 data points to draw a chart. Click !here to run the queries now.', array('!here' => l(t('here'), 'qmo_bugzilla/chart/' . $chart['id'] . '/run')));
      }
    }
    //bar chart
    elseif ($chart['type'] == CHART_TYPE_BAR) {
      $result = db_query("SELECT id, title, map_tag_values, count_total FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE chart_id = %d and enabled = 1", $chart['id']);
      //queries array
      $chart['q'] = array();
      $q_count = 0;
      while ($row = db_fetch_array($result)) {
        if ($row['count_total']) {
          $show_total = "";
        }
        else {
          $show_total = "AND NOT(label = '" . LABEL_TOTAL . "') ";
        }
        $q_count++;
        $chart['q'][$q_count] = $row;
        $query_r = "SELECT count, label FROM {" . QMO_BUGZILLA_TABLE_RESULT . "} WHERE (query_id = %d) " . $show_total . " GROUP BY query_id, label ORDER BY count DESC, added ";
        $result_r = db_query($query_r, $row['id']);
        $chart_results[$row['id']] = array();
        if ($result_r) {
          $count = 0;
          while ($row_r = db_fetch_object($result_r)) {
            $chart['q'][$q_count]['data'][$count]['count'] = $row_r->count;
            if ($row_r->label == LABEL_TOTAL) {
              $chart['q'][$q_count]['data'][$count]['label'] = $row['title'] . t('*');
            }
            else {
              $chart['q'][$q_count]['data'][$count]['label'] = $row_r->label;
            }
            $count++;
          }
          if (!$count) {
            $reply[] = array('empty-result');
          }
        }
        else {
          //no results found for this query
              $reply[] = array('empty-query');
        }
      }
      //get results
      $data = array( array() );
      $c_1 = 0;
      foreach($chart['q'] as $res) {
        foreach ($res['data'] as $bar) {
          $data[0][] = array(
                         '#value' => $bar['count'],
                         '#label' => $bar['label']
                       );
        }
        $c_1++;
      }
      $plot = array(
                '#plugin'    => 'openflashchart', // Open Flash Chart API will be used
                '#type'     => OPENFLASHMAP_BAR, // To show a bar chart
                '#height'   => $chart['height'],
                '#width'    => '100%',
                '#color'    => 'FFFFFF', // background color, in RRGGBB format
                '#title'    => $chart['title'], // The chart title
              );
      $plot = $plot + $data;
      $chart_display =  charts_chart($plot);
    }//end elseif bar chart
    elseif ($chart['type'] == CHART_TYPE_PIE) {
      $result = db_query("SELECT id, title, map_tag_values, count_total FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE chart_id = %d and enabled = 1", $chart['id']);
      //queries array
      $chart['q'] = array();
      $q_count = 0;
      while ($row = db_fetch_array($result)) {
        if ($row['count_total']) {
          $show_total = "";
        }
        else {
          $show_total = "AND NOT(label = '" . LABEL_TOTAL . "') ";
        }
        $q_count++;
        $chart['q'][$q_count] = $row;
        $query_r = "SELECT count, label FROM {" . QMO_BUGZILLA_TABLE_RESULT . "} WHERE (query_id = %d) " . $show_total . " GROUP BY query_id, label ORDER BY count DESC, added ";
        $result_r = db_query($query_r, $row['id']);
        $chart_results[$row['id']] = array();
        if ($result_r) {
          $count = 0;
          while ($row_r = db_fetch_object($result_r)) {
            $chart['q'][$q_count]['data'][$count]['count'] = $row_r->count;
            if ($row_r->label == LABEL_TOTAL) {
              $chart['q'][$q_count]['data'][$count]['label'] = $row['title'] . t('*');
            }
            else {
              $chart['q'][$q_count]['data'][$count]['label'] = $row_r->label;
            }
            $count++;
          }
          if (!$count) {
            $reply[] = array('empty-result');
          }
        }
        else {
          //no results found for this query
              $reply[] = array('empty-query');
        }
      }
      //get results
      $data = array( array() );
      $c_1 = 0;
      foreach($chart['q'] as $res) {
        foreach ($res['data'] as $bar) {
          $data[0][] = array(
                         '#value' => $bar['count'],
                         '#label' => $bar['label']
                       );
        }
        $c_1++;
      }
      $plot = array(
                '#plugin'   => 'openflashchart', // Open Flash Chart API will be used
                '#type'     => OPENFLASHMAP_PIE, // To show a pie chart
                '#height'   => $chart['height'], // in pixels
                '#width'    => '100%', // in pixels
                '#color'    => 'FFFFFF', // background color, in RRGGBB format
                '#title'    => $chart['title'], // The chart title
              );
      $plot = $plot + $data;
      $chart_display =  charts_chart($plot);
    }//end elseif pie chart
    else {
      //unknown chart type
      $chart_display = t('Unknown chart type');
    }
  }//end if ($hasdata)
  else {
    $chart_display = t('No queries have been run for this chart. Click !here to run them now, or wait until they are run at cron. ', array( '!here' => l(t('here'), 'qmo_bugzilla/chart/' . $chart['id'] . '/run')));
  }
  set_messages($reply);
  return $chart_display;
}

/**
 * Gets the column count to be specified to the QMOTimeplot object, based on data in results table
 * @param chart array(...)
 */
function get_timeplot_column_count($chart) {
  $count = 0;
  $result = db_query("SELECT COUNT(*) AS count FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE (map_tag_values = 0) and (chart_id = %d)", $chart['id']);
  $row = db_fetch_object($result);
  $count += $row->count;
  $result = db_query("SELECT COUNT(DISTINCT {" . QMO_BUGZILLA_TABLE_RESULT . "}.label) AS count FROM {" . QMO_BUGZILLA_TABLE_RESULT . "}
  LEFT OUTER JOIN {" . QMO_BUGZILLA_TABLE_QUERY . "} ON ( {" . QMO_BUGZILLA_TABLE_RESULT . "}.query_id = {" . QMO_BUGZILLA_TABLE_QUERY . "}.id )
      WHERE ({" . QMO_BUGZILLA_TABLE_QUERY . "}.chart_id = %d) AND ({" . QMO_BUGZILLA_TABLE_QUERY . "}.map_tag_values = 1)", $chart['id']);
//                  ORDER BY {" . QMO_BUGZILLA_TABLE_RESULT . "}.added, {" . QMO_BUGZILLA_TABLE_RESULT . "}.count");
  $row_2 = db_fetch_object($result);
  $count += $row_2->count;
  $result = db_query("SELECT COUNT(id) AS count FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE (chart_id = %d) AND (count_total = 1) AND (map_tag_values = 1)", $chart['id'] );
  $row_3 = db_fetch_object($result);
  $count -= ($row_3) ? $row_3->count : 0;
  return $count;
}

/**
 * Empty an existing chart.
 * @param id chart id
 * @param enabled boolean:
 *        - TRUE, only empty for enabled queries
 *        - FALSE, empty for all queries
 * @param silent boolean:
 *        - TRUE, no redirect
 *        - FALSE, redirects to referer url
 * @return TRUE on successs, FALSE otherwise
 */
function qmo_bugzilla_chart_empty($id, $enabled = FALSE, $silent = FALSE, $allow_access = FALSE) {
  global $user;
  if (!is_null($id) && !qmo_bugzilla_chart_exists($id, FALSE)) {
    return FALSE;
  }
  //get the chart type, title and user_id
  $query = "SELECT title, type, user_id FROM {" . QMO_BUGZILLA_TABLE_CHART . "} WHERE id = %d";
  $result = db_query($query, $id);
  if ($chart = db_fetch_array($result)) {
    //check access
    if (!$allow_access) {
      qmo_bugzilla_access_chart_empty($chart);
    }
  }
  if ($enabled) {
    $enabled = ' and enabled = 1 ';
  }
  else {
    $enabled = '';
  }
  $success = FALSE;
  $date = date(DATE_SQL_FORMAT);
  $reply = array();

  $query = "SELECT id FROM {" . QMO_BUGZILLA_TABLE_QUERY . "} WHERE chart_id = %d " . $enabled;
  $result = db_query($query, $id);
  //run the queries through the parser
  if ($result) {
    $xml_tags_string = variable_get('qmo_bugzilla_xml_tags', XML_TAGS);
    $maintag = variable_get('qmo_bugzilla_maintag', MAINTAG);
    $query_2 = "DELETE FROM {" . QMO_BUGZILLA_TABLE_RESULT . "} WHERE query_id IN (";
    $query_2_vars = array();

    $found= 0;
    while ($row = db_fetch_object($result)) {
      $query_2 .= "'%s',";
      $query_2_vars[] = $row->id;
      $found = 1;
    }
    
    if ($found) {
      $result_2 = db_query(substr($query_2, 0, -1) . ');', $query_2_vars);
        if ($result_2) {
          //update data file:
          set_timeplot_file($id);
          $reply[] = array('chart-emptied', 'vars' => array($chart['title']) );
          $success = TRUE;
        }
        else {
          $reply[] = array('no-query');
        }
    }
    else {
      $reply[] = array('empty-result');
      $success = TRUE;
    }
  }
  else {
    $reply[] = array('fail-chart-empty');
  }
  if ($silent == FALSE) {
    set_messages($reply);
    $goto = get_first_nonempty($_SERVER['HTTP_REFERER'], 'qmo_bugzilla');
    drupal_goto($goto);
  }
  return $success;
}