<?php

/**
 * @file
 * Handles incoming requests to fire off regularly-scheduled tasks (cron jobs).
 */

include_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
//drupal_cron_run();
  // If not in 'safe mode', increase the maximum execution time:
  if (!ini_get('safe_mode')) {
    set_time_limit(240);
  }

  // Fetch the cron semaphore
  $semaphore = variable_get('qmo_bugzilla_cron_semaphore', FALSE);

  if ($semaphore) {
    if (time() - $semaphore > 3600) {
      // Either cron has been running for more than an hour or the semaphore
      // was not reset due to a database error.
      watchdog('cron', 'QMO Bugzilla: Cron has been running for more than an hour and is most likely stuck.', array(), WATCHDOG_ERROR);

      // Release cron semaphore
      variable_del('qmo_bugzilla_cron_semaphore');
    }
    else {
      // Cron is still running normally.
      watchdog('cron', 'QMO Bugzilla: Attempting to re-run cron while it is already running.', array(), WATCHDOG_WARNING);
    }
    print "QMO Bugzilla: Cron did not run\n";
  }
  else {
    // Lock cron semaphore
    variable_set('qmo_bugzilla_cron_semaphore', time());

    // Iterate through the modules calling their cron handlers (if any):
    module_invoke('qmo_bugzilla', 'cron');

    // Record cron time
    variable_set('qmo_bugzilla_cron_last', time());
    watchdog('cron', 'QMO Bugzilla: Cron run completed.', array(), WATCHDOG_NOTICE);

    // Release cron semaphore
    variable_del('qmo_bugzilla_cron_semaphore');
    print "QMO Bugzilla: Cron ran successfully\n";
    // Return TRUE so other functions can check if it did run successfully
  }
